# discord-vk-music [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> 

## Installation

1. Install the supervisor:
http://supervisord.org/installing.html

2. Install nodejs:
https://docs.npmjs.com/getting-started/installing-node

3. Clone this repository, and run:
```sh
$ npm install
```

## Usage

```sh
$ ./start.sh
```

```sh
$ ./restart.sh
```

```sh
$ ./stop.sh
```
## License

MIT © [Feronetick]()


[npm-image]: https://badge.fury.io/js/discord-vk-music.svg
[npm-url]: https://npmjs.org/package/discord-vk-music
[travis-image]: https://travis-ci.org/feronetick/discord-vk-music.svg?branch=master
[travis-url]: https://travis-ci.org/feronetick/discord-vk-music
[daviddm-image]: https://david-dm.org/feronetick/discord-vk-music.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/feronetick/discord-vk-music
