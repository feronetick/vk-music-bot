'use strict';

const merge = require('merge');
const vsprintf = require('sprintf').vsprintf;
const sprintf = require('sprintf').sprintf;
const restler = require('restler');
const crypto = require('crypto');

class YandexMusic {

  constructor(config) {

    let defaultConfig = {
      bitrate_threshold: 96,
      ouath_code: {
        client_id: '0618394846eb4d9589a602f80ce013d6',
        client_secret: 'c13b3de8d9f5492caf321467c3520358',
      },
      oauth_token: {
        client_id: '23cabbbdc6cd418abb4b39c32c41195d',
        client_secret: '53bc75238f0c4d08a118e51fe9203300',
      },
      fake_device: {
        device_id: '377c5ae26b09fccd72deae0a95425559',
        uuid: '3cfccdaf75dcf98b917a54afe50447ba',
        package_name: 'ru.yandex.music'
      },
      user: {
        username: null,
        password: null,
        token: null,
        uid: null
      },
      api_url: {
        baseUrl: 'api.music.yandex.net',
        port: 443,
        scheme: 'https'
      },
      jsx_url: {
        baseUrl: 'music.yandex.ru/handlers',
        port: 443,
        scheme: 'https'
      },
      auth_url: {
        baseUrl: 'oauth.mobile.yandex.net',
        port: 443,
        scheme: 'https'
      },
      salt: 'XGRlBW9FXlekgbPrRHuSiA',
      hashTemplate: '%(salt)s%(path)s%(s)s',
      trackRealUrl: 'http://%(host)s/get-mp3/%(hash)s/%(ts)s%(path)s?track-id=%(track_id)s&from=service-10-track&similarities-experiment=default'
    };

    this.config = merge.recursive(true, defaultConfig, config);

    let defaultMethod = {
      url: null,
      method: 'get',
      args: {},
      prepareUrl: (url, args) => {
        let api_url = this.config.api_url;
        url = vsprintf("%s://%s:%s/%s", [api_url.scheme, api_url.baseUrl, api_url.port, url]);
        url = sprintf(url, args);
        return url;
      },
      prepareGet: (args) => {
        return args
      },
      preparePost: (args) => {
        return args
      },
      prepareResult: (data) => {
        if (data.result) data = data.result;
        return data;
      }
    };

    this.methods = {
      "account.status": merge.recursive(true, defaultMethod, {
        url: "account/status"
      }),
      "feed": merge.recursive(true, defaultMethod, {
        url: "feed"
      }),
      "genres": merge.recursive(true, defaultMethod, {
        url: "genres"
      }),
      "search": merge.recursive(true, defaultMethod, {
        url: "search",
        args: {
          type: {
            default: "all",
            required: false
          },
          text: {
            required: true
          },
          page: {
            required: true,
            default: 0
          },
          nococrrect: {
            required: true,
            default: false
          },
        }
      }),
      "user.playlists.list": merge.recursive(true, defaultMethod, {
        url: "users/%(user_id)s/playlists/list",
        args: {
          user_id: {
            default: false,
            required: true
          },
        }
      }),
      "user.playlists": merge.recursive(true, defaultMethod, {
        url: "users/%(user_id)s/playlists",
        args: {
          user_id: {
            default: false,
            required: true
          },
          playlist_id: {
            default: false,
            required: true
          },
        },
        prepareGet: (args) => {
          return {
            kinds: args.playlist_id,
            "rich-tracks": true,
          };
        }
      }),
      "user.playlist": merge.recursive(true, defaultMethod, {
        url: "users/%(user_id)s/playlists/%(playlist_id)s",
        args: {
          user_id: {
            default: false,
            required: true
          },
          playlist_id: {
            default: false,
            required: true
          },
        }
      }),
      "track.get": merge.recursive(true, defaultMethod, {
        url: "tracks/%(track_id)s",
        args: {
          track_id: {
            default: false,
            required: true
          }
        }
      }),
      "album.get": merge.recursive(true, defaultMethod, {
        url: "album.jsx",
        args: {
          album_id: {
            default: false,
            required: true
          }
        },
        prepareGet: (args) => {
          return {
            album: args.album_id,
            lang: 'ru',
          };
        },
        prepareUrl: (url, args) => {
          let api_url = this.config.jsx_url;
          url = vsprintf("%s://%s/%s", [api_url.scheme, api_url.baseUrl, url]);
          url = sprintf(url, args);
          return url;
        },
      }),
      "artist.get": merge.recursive(true, defaultMethod, {
        url: "artist.jsx",
        args: {
          artist_id: {
            default: false,
            required: true
          }
        },
        prepareGet: (args) => {
          return {
            artist: args.artist_id,
            what: 'tracks',
            overembed: 'false',
            lang: 'ru',
          };
        },
        prepareUrl: (url, args) => {
          let api_url = this.config.jsx_url;
          url = vsprintf("%s://%s/%s", [api_url.scheme, api_url.baseUrl, url]);
          url = sprintf(url, args);
          return url;
        },
      }),
      "playlist.get": merge.recursive(true, defaultMethod, {
        url: "playlist.jsx",
        args: {
          user_id: {
            default: false,
            required: true
          },
          playlist_id: {
            default: false,
            required: true
          },
        },
        prepareGet: (args) => {
          return {
            owner: args.user_id,
            kinds: args.playlist_id,
            light: 'false',
            overembed: 'false',
            lang: 'ru',
          };
        },
        prepareUrl: (url, args) => {
          let api_url = this.config.jsx_url;
          url = vsprintf("%s://%s/%s", [api_url.scheme, api_url.baseUrl, url]);
          url = sprintf(url, args);
          return url;
        },
      }),
      "download.info": merge.recursive(true, defaultMethod, {
        url: "tracks/%(track_id)s/download-info",
        args: {
          track_id: {
            default: false,
            required: true
          },
        },
        prepareResult: (data) => {
          let selected = false;
          for (let info of data.result) {
            if (
              info.gain
              && info.bitrateInKbps <= this.config.bitrate_threshold &&
              (!selected || info.bitrateInKbps > selected.bitrateInKbps)
            ) {
              selected = info;
            }
          }
          return selected;
        }
      }),
    };
  }

  api (method, options = {}) {

    if (!this.methods[method]) {
      return new Promise((resolve, reject) => { reject(`Method ${method} doesn't exists`) });
    }

    method = this.methods[method];

    let checkedOptions = {};

    if (method.args) {
      for (let optionName in method.args) {
        if (!method.args.hasOwnProperty(optionName)) continue;

        if (!options[optionName]) {

          if (method.args[optionName].default) {
            checkedOptions[optionName] = method.args[optionName].default;
          } else if (method.args[optionName].required) {
            return new Promise((resolve, reject) => { reject(`Method ${method} doesn't exists`) });
          }
        }
      }
    }

    return this.promiseRequest(
      method.method,
      method.prepareUrl(method.url, options),
      {
        query: method.prepareGet(options),
        data: method.preparePost(options),
        headers: this.getHeaders(),
        parser: restler.parsers.json,
        resultModifier: method.prepareResult
      }
    );
  }

  getAudioUrl (track, downloadInfoUrl) {
    return new Promise((resolve) => {
      this.promiseRequest(
        'get',
        downloadInfoUrl,
        {
          parser: restler.parsers.xml,
        }
      ).then(
        (data) => {
          resolve(this.getUrl(track, data['download-info']))
        },
        (error) => {
          resolve(false);
        }
      );
    });
  }

  getUrl (track, downloadInfo) {
    let replace = {
      s: downloadInfo.s[0],
      ts: downloadInfo.ts[0],
      path: downloadInfo.path[0].substring(1),
      salt: this.config.salt,
    };
    let secret = sprintf(this.config.hashTemplate, replace);
    let hash = crypto.createHash('md5').update(secret).digest('hex');
    replace = {
      host: downloadInfo.host[0],
      hash: hash,
      ts: downloadInfo.ts[0],
      path: downloadInfo.path[0],
      track_id: track.data.storageDir.split(".")[0]
    };
    return sprintf(this.config.trackRealUrl, replace);
  }

  init () {

    let auth_url = this.config.auth_url;
    let url = vsprintf("%s://%s:%s/1/token", [auth_url.scheme, auth_url.baseUrl, auth_url.port]);

    if (this.config.user.token && this.config.user.uid) {
      return new Promise((resolve) => { resolve(); });
    }

    return this.promiseRequest('post', url, {
      data: {
        grant_type: "password",
        username: this.config.user.username,
        password: this.config.user.password,
        client_id: this.config.ouath_code.client_id,
        client_secret: this.config.ouath_code.client_secret,
      },
      parser: restler.parsers.json,
    }).then(
      (data) => {

        let url = vsprintf("%s://%s:%s/1/token", [auth_url.scheme, auth_url.baseUrl, auth_url.port]);

        return this.promiseRequest('post', url, {
          query: {
            device_id: this.config.fake_device.device_id,
            uuid: this.config.fake_device.uuid,
            package_name: this.config.fake_device.package_name,
          },
          data: {
            grant_type: "x-token",
            access_token: data.access_token,
            client_id: this.config.ouath_code.client_id,
            client_secret: this.config.ouath_code.client_secret,
          },
          parser: restler.parsers.json,
        }).then(
          (token) => {
            this.config.user.token = token.access_token;
            this.config.user.uid = token.uid;

            return new Promise((resolve) => { resolve(); });
          }
        );
      }
    );
  }

  getHeaders () {
    return { 'Authorization' : `OAuth ${this.config.user.token}` };
  }

  promiseRequest (method, url, options) {
    return new Promise((resolve, reject) => {
      options.method = method;
      restler.request(url, options)
        .on('success', (data) => {
          if (typeof options.resultModifier == 'function') {
            data = options.resultModifier(data);
            if (data === false) {
              reject(data);
            }
          }
          resolve(data);
        })
        .on('fail', (error) => {
          if (!error) { error = "Request failed"; }
          reject(error);
        })
        .on('error', (error) => {
          if (!error) { error = "Request error"; }
          reject(error);
        })
        .on('abort', (error) => {
          if (!error) { error = "Request aborted"; }
          reject(error);
        })
        .on('timeout', (ms) => {
          reject(`Request timed out (${ms})`);
        });
    });
  }
}

module.exports = YandexMusic;
