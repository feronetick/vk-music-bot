var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "list",
  desc: "Отобразить текущий список воспроизведения",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);

    if (queue.empty()) {
      return `Список воспроизведения пуст`;
    }

    let limit = 2000;

    let currentTrack = queue.current();
    let messages = [`Текущий список воспроизведения. Всего ${queue.audios.length}`];
    let message = '';

    for (let i in queue.audios) {
      let index = i * 1;
      let track = queue.audios[i];
      index++;
      let bold = '';
      if (track == currentTrack) {
        bold = '**';
      }
      let msg = `\n#${bold}${index} ${track.formatName()}${bold}`;
      if (msg.length <= 1950) {
        if (msg.length + message.length > 1950) {
          messages.push(message);
          message = '';
        }
        message += msg;
      } else {
        messages.push(msg.substr(0, 1950));
        message = '';
      }
    }

    if (message.length > 0) {
      messages.push(message);
    }

    if (messages.length > 0) {
      // console.log(context.msg.channel.client.rest);
      context.msg.channel.client.rest.methods._sendMessageRequest(context.msg.channel, messages, null, false, false, () => {}, () => {});
    }

    return false;
  },

  args: [
  ],
  flags: [
  ]
});
