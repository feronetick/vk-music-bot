var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "recent",
  desc: "Показать последние запрошенные ссылки",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);

    let limit = 2000;

    if (queue.recent.length < 1) {
      return `Список пуст`;
    }

    let message = '';

    for (let recent of queue.recent) {
      //let msg = `\n${context.app.prefix}${context.app.separator}play ${recent}`;
      let msg = `\n${recent}`;

      if (msg.length <= 2000) {
        if (msg.length + message.length > limit) {
          context.msg.channel.sendMessage(message);
          message = '';
        }
        message += msg;
      } else {
        context.msg.channel.sendMessage("...");
      }
    }

    if (message.length > 0) {
      context.msg.channel.sendMessage(message);
    }

    return false;
  },

  args: [
  ],
  flags: [
  ]
});
