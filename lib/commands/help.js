var Clapp = require('../clapp.js');
const Queue = require('../queue.js');
const fs = require('fs');

module.exports = new Clapp.Command({
  name: "help",
  desc: "Эта справка",
  detail_desc: "Показывает справку по командам бота",
  fn: (argv, context) => {

    let msg = context.app._getHelp();
    return msg;
  },

  args: [
  ],
  flags: [
  ]
});
