var Clapp = require('../clapp.js');
const Collection = require('../../node_modules/discord.js/src/util/Collection');

module.exports = new Clapp.Command({
  name: "summon",
  desc: "Призывает бота в ваш текущий голосовой канал",
  fn: (argv, context) => {

    let author = context.msg.author;
    let voiceChannel = false;

    if (!context.msg.guild) {
      return `Я не могу воспроизводить музыку в личных сообщениях`;
    }

    if (!context.msg.guild.id) {
      return `Я не могу воспроизводить музыку в личных сообщениях`;
    }

    let channels = context.msg.guild.channels.findAll("type", "voice");

    for (let channel of channels) {
      if (
        (channel.members.exists("id", author.id) && argv.args.name == "")
        ||
        (argv.args.name == channel.name)
      ) {
        voiceChannel = channel;
        break;
      }
    }

    if (!voiceChannel) {
      if (argv.args.name) {
        return `Канал с названием ${argv.args.name} не найден`
      }
      return "Вы должны находиться в голосовом канале";
    }

    voiceChannel.join();
    return `Подключен к ${voiceChannel.name}`;
  },
  args: [
    {
      name: 'name',
      desc: 'Название канала',
      type: 'string',
      required: false,
      default: "",
    }
  ],
  flags: [
  ]
});
