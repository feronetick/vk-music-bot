var Clapp = require('../clapp.js');
const Queue = require('../queue.js');
const Resolver = require("../resolver.js");

module.exports = new Clapp.Command({
  name: "play",
  desc: "Воспроизведение",
  detail_desc: "Начинает воспроизведние списка треков. Если указан аргумент [url]," +
  "то список будет очищен и заполнен треками по ссылке. В противном случае воспроизведение" +
  "будет продолжено после паузы или команды [stop], если было остановлено.",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return `Я не подключен к голосовому каналу. Вы можете пригласить меня с помощью команды "${context.app.prefix}${context.app.separator}summon"`
    }

    let url = argv.args.url;
    let queue = Queue.getQueue(voiceConnection);

    if (url == '' && !queue.empty()) {
      if (queue.paused) {
        queue.resume();
      } else if (!queue.playing()) {
        queue.play(0, true);
      }
      return true;
    }

    let resolver = new Resolver(url, context);
    resolver.resolve().then(
      (audios) => {
        if (audios.length > 0) {
          queue.cleanup();
          queue.recent.push(url);
          if (queue.recent.length > 5) {
            queue.recent.shift();
          }
          queue.addAll(audios);
          queue.play();
        }
      },
      () => {}
    );

    return true;
  },

  args: [
    {
      name: 'url',
      desc: 'Ссылка на пользователя, группу или альбом. Если не указан, то продолжится воспроизведение текущего списка.',
      type: 'string',
      required: false,
      default: ''
    }
  ],
  flags: [
  ]
});
