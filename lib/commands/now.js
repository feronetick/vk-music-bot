var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "now",
  desc: "Показать название, номер и ссылку на текущий трек",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);

    return queue.getCurrentTrackMessage();
  },

  args: [
  ],
  flags: [
  ]
});
