var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "repeat",
  desc: "Включить или выключает повтор текущего списка",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);
    let value = argv.args.value;

    if (!['on', 'off', 'toggle'].indexOf(value) != -1) {
      value = 'toggle';
    }

    if (value == 'toggle') {
      if (queue.repeat) {
        value = 'off';
      } else {
        value = 'on';
      }
    }

    if (value == 'on') {
      queue.repeat = true;
      return `Повтор включен`;
    } else {
      queue.repeat  = false;
      return `Повтор выключен`;
    }
  },

  args: [
    {
      name: 'value',
      desc: '',
      alias: '',
      type: 'string',
      default: "toggle"
    }
  ],
  flags: [
  ]
});
