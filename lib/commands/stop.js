var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "stop",
  desc: "Остановить воспроизведение списка.",
  detail_desc: "Прекращает воспроизведение списка. При следующем запуске список будет начат с первого трека.",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);
    if (queue.index > 0)
      queue.index--;
    queue.pause();
    queue.stop();

    return true;
  },

  args: [
  ],
  flags: [
  ]
});
