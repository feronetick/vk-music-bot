var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "pause",
  desc: "Приостановить воспроизведение",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);
    queue.pause();

    return true;
  },

  args: [
  ],
  flags: [
  ]
});
