var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "volume",
  desc: "Установить громкость",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);
    let volume = argv.args.volume;
    queue.setVolume(volume);
  },

  args: [
    {
      name: 'volume',
      desc: 'Число от 0 до 100',
      alias: 'v',
      type: 'number',
      required: true,
    }
  ],
  flags: [
  ]
});
