var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "report",
  desc: "Включить уведомления в текущем канале",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);
    let value = argv.args.value;
    let channel = context.msg.channel;

    if (!['on', 'off', 'toggle'].indexOf(value) != -1) {
      value = 'toggle';
    }

    if (value == 'toggle') {
      if (queue.reportChannel == channel) {
        value = 'off';
      } else {
        value = 'on';
      }
    }

    if (value == 'on') {
      queue.reportChannel = channel;
      return `Уведомления для канала <#${channel.id}> включены`;
    } else {
      queue.reportChannel  = null;
      return `Уведомления выключены`;
    }
  },

  args: [
    {
      name: 'value',
      desc: 'Может быть "on" или "off". Уведомления могут быть включены только в одном канале.',
      alias: '',
      type: 'string',
      default: "toggle"
    }
  ],
  flags: [
  ]
});
