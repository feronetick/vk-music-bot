var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "resume",
  desc: "Продолжить воспроизведение после паузы",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);
    queue.resume();

    return true;
  },

  args: [
  ],
  flags: [
  ]
});
