var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "prev",
  desc: "Перейти на предыдущий трек в списке",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);
    queue.prev();

    return true;
  },

  args: [
  ],
  flags: [
  ]
});
