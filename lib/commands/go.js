var Clapp = require('../clapp.js');
const Queue = require('../queue.js');

module.exports = new Clapp.Command({
  name: "go",
  desc: "Переход к треку по номеру",
  detail_desc: "",
  fn: (argv, context) => {

    let voiceConnection = context.msg.guild.voiceConnection;

    if (!voiceConnection) {
      return true;
    }

    let queue = Queue.getQueue(voiceConnection);

    if (queue.go(argv.args.track)) {
      return false;
    }

    return `Не удалось найти трек под номером ${argv.args.track}`;
  },

  args: [
    {
      name: 'track',
      desc: 'Номер трека в текущем списке',
      alias: 't',
      type: 'number',
      required: true,
    }
  ],
  flags: [
  ]
});
