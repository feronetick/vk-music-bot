"use strict";

var config = require("../config");
var Clapp      = require('clapp');
var Table      = require('cli-table2');
var sprintf      = require('sprintf').sprintf;
var vsprintf      = require('sprintf').vsprintf;
let lang = config.lang || 'ru';
var str        = require('./lang/'+lang+'.js');

class App extends Clapp.App {
  constructor(options) {
    super(options);
  }

  _getHelp() {

    let max_length = 1950;
    let help = "```http\n";

    help += `${this.name}`;

    if (this.version) {
      help += ` v${this.version}`;
    }

    if (this.desc) {
      help += `\n\n${this.desc}`;
    }

    help += `\n\n${str.help.usage} ${this.prefix}${this.separator}${str.help.command}\n\n`;

    help += str.help.command_list + "\n\n";

    let name_max_length = 0;

    for (let name in this.commands) {
      if (name.length > name_max_length) {
        name_max_length = name.length;
      }
    }

    for (let name in this.commands) {
      let spaces = " ";
      spaces = spaces.repeat(name_max_length - name.length + 1);
      help += `${name}:${spaces}${this.commands[name].desc}\n`;
    }

    help +=
      '\n\n' +
      str.help.get_detail_help + ' ' + this.prefix + this.separator + str.help.command + ' --help' +
      '```'
    ;

    return help;
  }
}

class Command extends Clapp.Command {
  constructor(options) {
    super(options);
    this.detail_desc = options.detail_desc || null;
  }

  _getHelp(app) {

    let max_length = 1950;
    let help = "```markdown\n";

    help += `#${str.help.command_name} ${this.name}\n\n`;

    if (this.detail_desc) {
      help += `${this.detail_desc}\n\n`;
    } else if (this.desc) {
      help += `${this.desc}\n\n`;
    }

    help += `#${str.help.usage}\n\n${app.prefix}${app.separator}${this.name}`;

    let hasArgs = false;

    for (let name in this.args) {
      let arg = this.args[name];
      help += ` [${name}]`;
      hasArgs = true;
    }

    help += `\n\n`;

    if (hasArgs) {

      help += `#${str.help.available_args}\n\n`;

      for (let name in this.args) {
        let arg = this.args[name];
        if (arg.required) {
          help += `* `;
        } else {
          help += `- `;
        }
        help += `${name}`;
        if (arg.desc) {
          help += `: ${arg.desc}`;
        }
        help += `\n\n`;
      }

      help += `${str.help.required_desc}\n\n`;
    }

    let hasFlags = false;

    for (let name in this.flags) {
      hasFlags = true;
    }

    if (hasFlags) {

      help += `#${str.help.available_flags}\n\n`;

      for (let name in this.flags) {
        let flag = this.flags[name];
        help += `- --`;
        help += `${name}`;
        if (flag.desc) {
          help += `: ${flag.desc}`;
        }
        help += `\n\n`;
      }
    }

    help += '```';

    return help;
  }
}

module.exports = {
  App: App,
  Command: Command
};
