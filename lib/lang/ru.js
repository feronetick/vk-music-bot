module.exports = {

  // Help

  help: {
    command_name: 'Команда',
    usage: 'Использование',
    command: '[команда]',
    command_list: 'Список команд',
    get_detail_help: 'Для получения подробной справки введите:',
    available_args: 'Доступные аргументы',
    available_flags: 'Доступные флаги',
    required_desc: 'Аргументы, отмеченные звездочкой (*), обязательны.',
  },
};
