'use strict';

const Audio = require('./audio.js');
const request = require("request");
const fs = require('fs');
const VoiceConnection = require('../node_modules/discord.js/src/client/voice/VoiceConnection.js');
const StreamDispatcher = require('../node_modules/discord.js/src/client/voice/dispatcher/StreamDispatcher');
const cfg = require('../config.js');
const sprintf = require('sprintf').sprintf;

class Queue {

  constructor(voiceConnection) {
    if (voiceConnection instanceof VoiceConnection) {
      this.connection = voiceConnection;
      this.audios = [];
      this.recent = [];
      this.index = 0;
      this.repeat = false;
      this.reportChannel = null;
      this.volume = 1;
      this.timer = 0;
      this.paused = false;
      this.previousStream = null;
    } else {
      throw new Error();
    }
  }

  saveState () {
    let filename = `${cfg.cache_dir}/state_${this.connection.channel.guild.id}.json`;

    if (!fs.existsSync(cfg.cache_dir)) {
      fs.mkdirSync(cfg.cache_dir);
    }

    if (fs.existsSync(filename)) {
      fs.unlinkSync(filename);
    }

    fs.writeFileSync(filename, JSON.stringify(this));
  }

  toJSON () {
    let object = {
      channel_id: this.connection.channel.id,
      index: (this.index > 0 && !this.paused ? this.index - 1 : this.index),
      repeat: this.repeat,
      volume: this.volume,
      recent: this.recent,
      guild_id: this.connection.channel.guild.id,
      playing: this.playing(),
      paused: this.paused,
      timer: this.timer,
      audios: this.audios
    };

    if (this.reportChannel) {
      object.reportChannel = this.reportChannel.id;
    }

    return object;
  }

  static loadState (object, bot) {
    if (!object.guild_id) {
        return false;
    }
    if (!object.channel_id) {
      return false;
    }
    let guild = bot.guilds.find('id', object.guild_id);
    if (!guild) {
      return false;
    }
    let channel = guild.channels.find('id', object.channel_id);
    if (!channel || channel.type != 'voice') {
      return false;
    }
    return channel.join().then((voiceConnection) => {
      let queue = this.getQueue(voiceConnection);
      if (object.reportChannel) {
        let reportChannel = guild.channels.find('id', object.reportChannel);
        if (reportChannel) {
          queue.reportChannel = reportChannel;
        }
      }
      if (object.volume) {
        queue.volume = object.volume;
      }
      if (object.paused) {
        queue.paused = object.paused;
      }
      if (object.audios) {
        for (let audio of object.audios) {
          queue.add(new Audio(audio));
        }
      }
      if (object.repeat) {
        queue.repeat = object.repeat;
      }
      if (object.recent) {
        queue.recent = object.recent;
      }
      if (object.index) {
        queue.index = object.index*1;
      }
      if (object.timer) {
        queue.timer = object.timer;
      }
      if (!object.paused && object.playing) {
        queue.play(object.timer);
      }

      return true;
    }, () => {
      return false;
    });
  }

  add (audio) {
    if (audio instanceof Audio) {
      this.audios.push(audio);
    }
  }

  addAll (audios) {
    for (let audio of audios) {
      this.add(audio, false);
    }
  }

  play (seek = 0) {
    if (this.index >= this.audios.length) {
      this.index = 0;
    }
    if (this.index < this.audios.length) {
      let audio = this.audios[this.index];
      this.paused = false;
      audio.getUrl().then(
        (url) => {
          this.index++;
          let stream = request(url);

          stream.on('timeout', () => {
            this.index--;
            this.resume();
          });
          stream.on('error', () => {
            this.index--;
            this.resume();
          });

          if (this.connection.player.dispatcher) {
            this.connection.player.dispatcher.pause();
          }
          if (this.previousStream) {
            this.previousStream.response.connection.destroy();
          }
          this.previousStream = stream;
          if (this.reportChannel) {
            this.reportChannel.setTopic(this.getCurrentTrackMessage(audio));
            // this.reportChannel.sendMessage(this.getCurrentTrackMessage(audio));
          }
          this.timer = seek;
          this.connection.player.playUnknownStream(stream, {passes: 4, seek: seek, volume: this.volume});
          this.startPlayingListener();
        },
        (error) => {
          this.index++;
        }
      );
    }
  }

  startPlayingListener (delay = 500) {
    clearInterval(this.interval);
    this.interval = setInterval(() => {
      if (!this.playing()) {
        if (this.index < this.audios.length || this.repeat) {
          this.play();
        }
      } else {
        this.timer += delay / 1000;
      }
    }, delay)
  }

  next () {
    if (this.index >= this.audios.length && this.repeat) {
      this.index = 0;
    }
    if (this.audios[this.index]) {
      clearInterval(this.interval);
      this.play();
    }
  }

  prev () {
    let index = this.index-2;
    if (this.audios[index]) {
      this.index = index;
      clearInterval(this.interval);
      this.play();
    }
  }

  go (index) {
    index = index - 1;
    if (!this.audios[index]) {
      return false;
    }
    this.index = index;
    clearInterval(this.interval);
    this.play();
    return true;
  }

  stop () {
    this.timer = 0;
    this.index = 0;
    if (this.reportChannel) {
      this.reportChannel.setTopic(this.getCurrentTrackMessage());
    }
    if (this.connection.player.dispatcher) {
      this.connection.player.dispatcher.paused = false;
      this.connection.player.dispatcher.end();
      this.connection.player.dispatcher.stream.destroy();
    }
    if (this.previousStream) {
      this.previousStream.response.connection.destroy();
      this.previousStream = null;
    }
    this.paused = false;
  }

  pause () {
    clearInterval(this.interval);
    this.paused = true;
    this.index--;
    if (this.connection.player.dispatcher) {
      this.connection.player.dispatcher.paused = false;
      this.connection.player.dispatcher.end();
      this.connection.player.dispatcher.stream.destroy();

    }
  }

  resume () {
    this.play(this.timer);
  }

  cleanup () {
    this.audios = [];
    this.index = 0;
    this.stop();
  }

  current () {
    let index = this.index;
    if (this.playing()) {
      index = this.index-1;
    }
    if (this.audios[index]) {
      return this.audios[index];
    } else {
      return this.audios[0];
    }
  }

  playing () {
    return this.connection.speaking;
  }

  empty () {
    return !this.audios[0];
  }

  getCurrentTrackMessage (audio = null) {
    let track;
    if (audio == null) {
      if (this.empty()) return `Список воспроизведения пуст`;
      track = this.current();
      if (!track) return `Список воспроизведения пуст`;
    } else {
      track = audio;
    }

    let index = this.audios.indexOf(track) + 1;
    let message = '';
    if (index == 0) {
      message = `**${track.formatName()}**`;
    } else {
      message = `#${index} **${track.formatName()}**`;
    }
    // let url = track.formatUrl();
    // if (url) message += `\nСкачать: ${url}`;
    return message;
  }

  setVolume (volume) {
    if (volume > 100) volume = 100;
    if (volume < 0) volume = 0;
    this.volume = volume / 100;
    if (this.connection.player.dispatcher) {
      this.connection.player.dispatcher.setVolume(this.volume);
    }
  }

  static getQueue (voiceConnection) {
    let guild_id = voiceConnection.channel.guild.id;
    if (!Queue.instances) {
      Queue.instances = {};
    }
    if (!Queue.instances.hasOwnProperty(guild_id)) {
      Queue.instances[guild_id] = new Queue(voiceConnection);
    }

    return Queue.instances[guild_id];
  }

  static getQueues () {
    if (!Queue.instances) {
      Queue.instances = {};
    }
    return Queue.instances;
  }
}

module.exports = Queue;
