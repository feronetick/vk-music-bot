'use strict';
const Promise = require('Promise').default;
const Audio = require('../audio.js');

class VKResolver {
  constructor(url, context) {
    this.api = context.vk;
    this.url = url;
    this.regex = {
      audio: /vk.com\/audios([-]?\d+)/,
      id: /vk.com\/(?:id)?(\w+)/,
      album: /vk.com\/album_id=(\d+)/
    };
  }

  static get(url, context) {
    return new VKResolver(url, context);
  }

  resolve () {

    return new Promise((resolve, reject) => {

      this.getFromUrl().then(
        (result) => {
          let audios = [];

          for (let item of result.response.items) {
            let audio = {
              url: item.url,
              id: `${item.owner_id}_${item.id}`,
              artist: item.artist,
              title: item.title,
              duration: item.duration,
              provider: 'vkontakte'
            };
            audios.push(new Audio(audio));
          }

          resolve(audios);
        },
        (error) => {

          reject(error);
        }
      );
    });
  }

  getFromUrl () {
    if (this.regex.audio.test(this.url)) {
      return this.getFromAudio();
    } else if (this.regex.id.test(this.url)) {
      return this.getFromId()
    } else {
      return new Promise((resolve, reject) => {
        reject();
      });
    }
  }

  getFromId () {
    return new Promise((resolve, reject) => {

      let matches = this.regex.id.exec(this.url);
      let id = matches[1];

      this.api.api("users.get", {user_ids: id, name_case: "Gen"}, (error, result) => {

        if (!error) {

          this.api.api("audio.get", {owner_id: result.response[0].id}, (error, result) => {

            if (error) {
              reject(error);
            } else {
              resolve(result);
            }

          });

        } else {

          this.api.api("groups.getById", {group_id: id}, (error, result) => {

            if (error) {
              reject(error);
            } else {

              this.api.api("audio.get", {owner_id: result.response[0].id}, (error, result) => {
                if (error) {
                  reject(error);
                } else {
                  resolve(result);
                }
              });

            }
          });
        }
      });
    });
  }

  getFromAudio () {

    return new Promise((resolve, reject) => {

      let params = [];

      if (this.regex.album.test(this.url)) {
        let matches = this.regex.album.exec(this.url);
        params.album_id = matches[1];
      }

      let matches = this.regex.audio.exec(this.url);
      params.owner_id = matches[1];

      this.api.api("audio.get", params, (error, result) => {

        if (error) {
          reject(error);
        } else {
          resolve(result);
        }

      });
    });
  }

  static getUrl (audio) {
    return new Promise((resolve, reject) => {
      let cfg = require('../../config.js');
      const VK = require('vk-api');
      var vk = new VK({
        appID: cfg.vk.app_id,
        appSecret: cfg.vk.app_secret,
        mode: "sig",
      });

      vk.setToken(cfg.vk.token);

      vk.api('audio.getById', {audios: audio.id}, (error, result) => {
        if (error) {
          return reject(new Error(error));
        }
        if (result.response && result.response[0]) {
          let item = result.response[0];
          return resolve(item.url);
        }
        reject(new Error(''));
      });
    });
  }
}

module.exports = VKResolver;
