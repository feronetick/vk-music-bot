'use strict';
const Promise = require('promise');
const Audio = require('../audio.js');

class YMResolver {
  constructor(url, context) {
    this.api = context.yandex;
    this.url = url;
    this.cfg = context.cfg;
    this.regex = {
      playlist: /music\.yandex\.[\w]+\/users\/([\w-]+)\/playlists\/(\d+)/,
      album: /music\.yandex\.[\w]+\/album\/(\d+)[\/]?$/,
      track: /music\.yandex\.[\w]+\/.*\/track\/(\d+)[\/]?$/,
      artist: /music\.yandex\.[\w]+\/artist\/(\d+)/,
    };
  }

  static get(url, context) {
    return new YMResolver(url, context);
  }

  resolve () {

    return new Promise((resolve, reject) => {

      if (this.regex.playlist.test(this.url)) {

        this.getPlaylist(resolve, reject);

      } else if (this.regex.track.test(this.url)) {

        this.getTrack(resolve, reject);

      } else if (this.regex.album.test(this.url)) {

        this.getAlbum(resolve, reject);

      } else if (this.regex.artist.test(this.url)) {

        this.getArtist(resolve, reject);

      } else {
        reject();
      }
    });
  }

  getArtist (resolve, reject) {
    let matches = this.regex.artist.exec(this.url);
    let artistId = matches[1];

    let audios = [];

    this.api.init().then(() => {
      this.api.api('artist.get', {artist_id: artistId}).then((data) => {

        let tracks = data.tracks;

        for (let j in tracks) {
          let track = tracks[j];

          let artists = [];
          for (let i in track.artists) {
            let artist = track.artists[i];
            artists.push(artist.name);
          }

          audios.push(new Audio({
            id: track.id,
            title: track.title,
            artist: artists.join(", "),
            provider: 'yandex',
            data: {
              storageDir: track.storageDir
            }
          }));
        }

        // console.log(audios);

        if (audios.length > 0) {
          resolve(audios);
        } else {
          reject('');
        }

      });
    });
  }

  getAlbum (resolve, reject) {
    let matches = this.regex.album.exec(this.url);
    let albumId = matches[1];

    let audios = [];

    this.api.init().then(() => {
      this.api.api('album.get', {album_id: albumId}).then((data) => {

        for (let k in data.volumes) {

          let tracks = data.volumes[k];

          for (let j in tracks) {
            let track = tracks[j];

            let artists = [];
            for (let i in track.artists) {
              let artist = track.artists[i];
              artists.push(artist.name);
            }

            audios.push(new Audio({
              id: track.id,
              title: track.title,
              artist: artists.join(", "),
              provider: 'yandex',
              data: {
                storageDir: track.storageDir
              }
            }));
          }
        }

        if (audios.length > 0) {
          resolve(audios);
        } else {
          reject('');
        }

      });
    });
  }

  getTrack (resolve, reject) {
    let matches = this.regex.track.exec(this.url);
    let trackId = matches[1];

    let audios = [];

    this.api.init().then(() => {
      this.api.api('track.get', {track_id: trackId}).then((data) => {

        let track = data[0];

        let artists = [];
        for (let i in track.artists) {
          let artist = track.artists[i];
          artists.push(artist.name);
        }
        audios.push(new Audio({
          id: track.id,
          title: track.title,
          artist: artists.join(", "),
          provider: 'yandex',
          data: {
            storageDir: track.storageDir
          }
        }));

        if (audios.length > 0) {
          resolve(audios);
        } else {
          reject('');
        }

      });
    });
  }

  getPlaylist (resolve, reject) {
    let matches = this.regex.playlist.exec(this.url);
    let userId = matches[1];
    let playlistId = matches[2];

    let audios = [];

    this.api.init().then(() => {
      this.api.api('playlist.get', {user_id: userId, playlist_id: playlistId}).then((data) => {

        let tracks = data.playlist.tracks;

        for (let j in tracks) {
          let track = tracks[j];

          let artists = [];
          for (let i in track.artists) {
            let artist = track.artists[i];
            artists.push(artist.name);
          }

          audios.push(new Audio({
            id: track.id,
            title: track.title,
            artist: artists.join(", "),
            provider: 'yandex',
            data: {
              storageDir: track.storageDir
            }
          }));
        }

        // console.log(audios);

        if (audios.length > 0) {
          resolve(audios);
        } else {
          reject('');
        }

      });
    });
  }

  static getUrl (audio) {
    return new Promise((resolve, reject) => {
      let cfg = require('../../config.js');
      const Yandex  = require('../api/yandex/api.js');
      let yandex = new Yandex(cfg.yandex_music);
      yandex.init().then(
        () => {
          yandex.api('download.info', {track_id: audio.id}).then(
            (downloadInfo) => {
              yandex.getAudioUrl(audio, downloadInfo.downloadInfoUrl).then(
                (url) => {
                  resolve(url);
                },
                () => {
                  reject(new Error(''))
                }
              );
            },
            () => {
              reject(new Error(''))
            }
          );
        },
        (error) => {
          reject(new Error(error))
        }
      );
    });
  }
}

module.exports = YMResolver;
