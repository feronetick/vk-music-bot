'use strict';

const fs      = require('fs');
const Clapp   = require('./clapp.js');
const cfg     = require('../config.js');
const pkg     = require('../package.json');
const Discord = require('discord.js');
const VK      = require('vk-api');
const Queue   = require('./queue.js');
const Yandex  = require('./api/yandex/api.js');
const Promise = require('promise');
const bot     = new Discord.Client();

var vk = new VK({
  appID: cfg.vk.app_id,
  appSecret: cfg.vk.app_secret,
  mode: "sig",
});

vk.setToken(cfg.vk.token);

var yandex = new Yandex(cfg.yandex_music);

var app = new Clapp.App({
  name: cfg.name,
  desc: pkg.description,
  prefix: cfg.prefix,
  separator: cfg.separator,
  version: pkg.version,
  onReply: (msg, context) => {
    // Fired when input is needed to be shown to the user.

    context.msg.channel.sendMessage('\n' + msg);
  }
});

// Load every command in the commands folder
fs.readdirSync('./lib/commands/').forEach(file => {
  app.addCommand(require("./commands/" + file));
});

bot.on('message', msg => {
  // Fired when someone sends a message

  if (app.isCliSentence(msg.content) && msg.author != bot.user) {
    app.parseInput(msg.content, {
      msg: msg,
      bot: bot,
      app: app,
      vk: vk,
      yandex: yandex,
      // Keep adding properties to the context as you need them
    });
  }
});

bot.login(cfg.token).then(() => {
  if (fs.existsSync(cfg.cache_dir))
  fs.readdirSync(cfg.cache_dir).forEach(file => {
    if (file.endsWith(".json")) {
      let filename = `${cfg.cache_dir}/${file}`;
      let json = fs.readFileSync(filename);
      let object = JSON.parse(json);
      Queue.loadState(object, bot);
    }
  });
});

var exitHandler = () => {
  var queues = Queue.getQueues();
  var arr = [];
  for (let i in queues) {
    let queue = queues[i];
    queue.saveState();
    if (queue.reportChannel) {
      arr.push(queue.reportChannel.setTopic('Offline :('));
    }
  }
  Promise.all(arr).then(
    () => {
      process.exit(0);
    },
    () => {
      process.exit(0);
    }
  );
};

process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
process.on('SIGTERM', exitHandler);
process.on('uncaughtException', function (error) {
  console.log(error.stack);
});
// process.on('SIGKILL', exitHandler);


