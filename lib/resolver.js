'use strict';
const fs = require('fs');
const Promise = require('Promise').default

class Resolver {

  constructor(url, context) {
    this.resolvers = [];
    fs.readdirSync('./lib/resolvers/').forEach(file => {
      this.resolvers.push(require("./resolvers/" + file).get(url, context));
    });
  }

  resolve () {
    this.iterator = 0;
    return new Promise((resolve, reject) => {
      this._next(resolve, reject);
    });
  }

  _next (resolve, reject) {
    if (this.resolvers[this.iterator]) {
      this.resolvers[this.iterator].resolve().then(
        (audios) => {
          resolve(audios);
        },
        (error) => {
          setTimeout( () => {
            this._next(resolve, reject);
          }, 0)
        }
      );
      this.iterator++;
    } else {
      reject();
    }
  }
}

module.exports = Resolver;
