'use strict';

class Audio {

  constructor(data) {
    if (data.id)
      this.id = data.id;
    if (data.artist)
      this.artist = data.artist;
    if (data.title)
      this.title = data.title;
    if (data.url)
      this.url = data.url;
    if (data.duration)
      this.duration = data.duration;
    if (data.provider)
      this.provider = data.provider;
    if (data.data)
      this.data = data.data;
  }

  formatName () {
    if (this.artist && this.title)
      return `${this.artist} - ${this.title}`;
    if (this.artist)
      return `${this.artist}`;
    if (this.title)
      return `${this.title}`;
  }

  formatUrl () {
    if (this.url) {
      return this.url.split('?')[0];
    }
  }

  getUrl () {
    return new Promise((resolve, reject) => {
      if (this.provider) {
        let provider = require(`./resolvers/${this.provider}.js`);
        provider.getUrl(this).then(
          (url) => {
            resolve(url);
          },
          (error) => {
            reject(error);
          }
        );
      } else {
        reject(new Error('Provider not exists'));
      }
    });

  }

  toJSON () {
    return {
      id: this.id,
      artist: this.artist,
      title: this.title,
      url: this.url,
      duration: this.duration,
      provider: this.provider,
      data: this.data,
    };
  }
}


module.exports = Audio;
