module.exports = {

  // Yandex music inner configuration may be rediclared there
  yandex_music: {
    user: {
      username: "",
      password: ""
    }
  },

  // We can't authorize user manually
  // You need specify existing offline token
  // See https://vk.com/dev/access_token
  vk: {
    app_id: 111111,
    app_secret: "",
    token: "",
  },

  // Your bot name. Typically, this is your bot's username without the discriminator.
  // i.e: if your bot's username is MemeBot#0420, then this option would be MemeBot.
  name: "VK Music Bot",

  // The bot's command prefix. The bot will recognize as command any message that begins with it.
  // i.e: "= foo" will trigger the command "foo",
  //      whereas "VK Music Bot foo" will do nothing at all.
  prefix:  "=",
  separator: "",

  // Your bot's user token. If you don't know what that is, go here:
  // https://discordapp.com/developers/applications/me
  // Then create a new application and grab your token.
  token: "",

  // Only ru available
  lang: 'ru',

  // Cache dir contains saved data to restore after restart application
  cache_dir: './cache',
};
