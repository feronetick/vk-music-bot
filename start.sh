#!/bin/bash

if [ ! -d "logs" ]
then
    mkdir "logs"
fi

if pgrep "supervisord" > /dev/null
then
    /usr/bin/supervisorctl start vk-music-bot
else
    /usr/bin/supervisord -c "supervisord.conf"
fi

